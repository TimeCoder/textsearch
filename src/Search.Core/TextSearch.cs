﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Search.Core.Index;
using Search.Core.IO;
using Search.Core.Tokenizer;
using Search.Core.Utils;

namespace Search.Core
{
    /// <summary>
    /// Класс поиска
    /// </summary>
    public class TextSearch : IDisposable
    {
        private readonly DiskMonitor _diskMonitor;
        private readonly UniqueConcurentQueue<string> _queue;
        private readonly Indexer _index;
        private readonly ITokenizer _tokenizer;

        public TextSearch()
        {
            _diskMonitor = new DiskMonitor();
            _queue = new UniqueConcurentQueue<string>();
            _index = new Indexer();
            _tokenizer = new WhitespaceTokenizer();

            _diskMonitor.OnFileNeedIndex += filename => _queue.Enqueue(filename);
                
            _queue.QueueChange += item => Task.Factory.StartNew(() =>
            {
                var file = _queue.Dequeue();

                if (file != null)
                {
                    using (var reader = new FastTextReader(file))
                    {
                        if (!reader.FileIsLock)
                            _index.IndexDocument(file, reader.Stream, _tokenizer);
                    }
                }

            });
        }
        
        public void AddFolderOrFile(string folder, string filter = "*.txt")
        {
            _diskMonitor.AddFolderOrFile(folder, filter);
        }

        public IEnumerable<string> Find(string word)
        {
            return _index.Find(word);
        }

        public int IndexCount
        {
            get { return _index.IndexCount; }
        }

        public void Dispose()
        {
            _diskMonitor.Dispose();
        }
    }
}
