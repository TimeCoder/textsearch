﻿using System.Collections.Generic;
using System.IO;

namespace Search.Core.Tokenizer
{
    /// <summary>
    /// Самый простой токенайзер, сегментирующий текст по пробелам
    /// </summary>
    internal class WhitespaceTokenizer : ITokenizer
    {
        public IEnumerable<string> Tokenize(TextReader reader)
        {
            // Размер буфера - оптимальный с точки зрения особенностей работы кэша HDD
            const int bufSize = 64*1024;
            var buffer = new char[bufSize];
            var readed = bufSize;

            while (readed == bufSize)
            {
                readed = reader.ReadBlock(buffer, 0, bufSize);

                var tokenBeginIndex = 0;
                var inToken = (buffer[0] != ' ');

                for (var i = 0; i <= readed; i++)
                {
                    // Закончился токен
                    if (inToken && (i==readed || buffer[i] == ' '))
                    {
                        inToken = false;
                        yield return new string(buffer, tokenBeginIndex, i - tokenBeginIndex);
                    }

                    // Начался токен
                    if (!inToken && (i == readed || buffer[i] != ' '))
                    {
                        inToken = true;
                        tokenBeginIndex = i;
                    }
                }
            }
        }
    }
}
