﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Search.Core.Tokenizer
{
    /// <summary>
    /// Интерфейс для разнообразных токенайзеров
    /// </summary>
    internal interface ITokenizer
    {
        IEnumerable<string> Tokenize(TextReader reader);
    }
}
