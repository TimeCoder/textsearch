﻿using System;
using System.IO;

namespace Search.Core.IO
{
    /// <summary>
    /// Класс для быстрого (относительно, конечно) чтения текстового файла
    /// </summary>
    internal class FastTextReader : IDisposable
    {
        private readonly FileStream _fileStream;
        private readonly BufferedStream _bufferedStream;
        private readonly StreamReader _streamReader;

        public FastTextReader(string filename) 
        {
            try
            {
                _fileStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            catch (IOException)
            {
                FileIsLock = true;
                return;
            }
            
            _bufferedStream = new BufferedStream(_fileStream);
            _streamReader = new StreamReader(_bufferedStream);
        }


        public TextReader Stream
        {
            get { return _streamReader; }
        }

        public bool FileIsLock { get; private set; }


        public void Dispose()
        {
            if (_streamReader != null) 
                _streamReader.Dispose();

            if (_bufferedStream != null) 
                _bufferedStream.Dispose();

            if (_fileStream != null) 
                _fileStream.Dispose();
        }
    }
}
