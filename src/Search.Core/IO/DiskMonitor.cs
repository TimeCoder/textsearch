﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Security.Permissions;

namespace Search.Core.IO
{
    /// <summary>
    /// Класс для отслеживания изменений на диске
    /// </summary>
    internal class DiskMonitor : IDisposable
    {
        private readonly ConcurrentBag<FileSystemWatcher> _watchers = new ConcurrentBag<FileSystemWatcher>();

        public delegate void FileChangeHandler(string filename);
        public event FileChangeHandler OnFileNeedIndex;

        private void RaiseFileNeedIndex(string filename)
        {
            var handler = OnFileNeedIndex;
            if (handler != null) handler(filename);
        }
       
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void AddFolderOrFile(string folder, string filter)
        {
            EnumerateFiles(folder, filter);

            var watcher = new FileSystemWatcher();
            _watchers.Add(watcher);
        
            watcher.Path = folder;
            watcher.IncludeSubdirectories = true;

            watcher.NotifyFilter = 
                NotifyFilters.LastAccess | 
                NotifyFilters.LastWrite | 
                NotifyFilters.FileName |
                NotifyFilters.DirectoryName;

            watcher.Filter = filter;

            watcher.Changed += OnChanged;
            watcher.Created += OnChanged;
            watcher.Deleted += OnChanged;
            watcher.Renamed += OnRenamed;

            watcher.EnableRaisingEvents = true;
        }


        private void EnumerateFiles(string folder, string filter)
        {
            if (!filter.Contains("*"))
            {
                RaiseFileNeedIndex(Path.Combine(folder, filter));
                return;
            }

            foreach (var file in Directory.EnumerateFiles(folder, filter, SearchOption.AllDirectories))
            {
                RaiseFileNeedIndex(file);
            }
        }

        
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Deleted)
            {
                RaiseFileNeedIndex(e.FullPath);
            }
        }


        private void OnRenamed(object source, RenamedEventArgs e)
        {
            RaiseFileNeedIndex(e.FullPath);
        }


        public void Dispose()
        {
            foreach (var watcher in _watchers)
            {
                watcher.Dispose();
            }
        }
    }
}
