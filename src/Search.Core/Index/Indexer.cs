﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Search.Core.Tokenizer;
using Search.Core.Utils;

namespace Search.Core.Index
{
    /// <summary>
    /// Класс для индексации документов
    /// </summary>
    internal class Indexer
    {
        // Индекс хранится в виде словаря, ключ - слово, значение - список документов (без дубликтов), где оно встречается 
        private readonly ConcurrentDictionary<string, ConcurrentSet<string>> _index;

        public Indexer()
        {
            _index = new ConcurrentDictionary<string, ConcurrentSet<string>>();
        }

        public void IndexDocument(string documentName, TextReader reader, ITokenizer tokenizer)
        {
            foreach (var token in tokenizer.Tokenize(reader))
            {
                _index.AddOrUpdate(token,
                    key => new ConcurrentSet<string> { documentName },
                    (key, set) =>
                    {
                        set.Add(documentName);
                        return set;
                    });
            }
        }

        public IEnumerable<string> Find(string query)
        {
            ConcurrentSet<string> node;
            if (!_index.TryGetValue(query, out node)) 
                return null;

            return node.ToList();
        }

        public int IndexCount
        {
            get { return _index.Count; }
        }
    }
}
