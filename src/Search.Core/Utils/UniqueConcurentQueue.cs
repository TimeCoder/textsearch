﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Search.Core.Utils
{
    /// <summary>
    /// Потокобезопасная очередь без дубликатов
    /// </summary>
    internal class UniqueConcurentQueue<T> where T : class, IComparable
    {
        private readonly Queue<T> _items = new Queue<T>(); 
        private readonly object _sync = new object();

        public delegate void QueueChangeHandler(T newItem);
        public event QueueChangeHandler QueueChange;

        private void RaiseQueueChange(T newItem)
        {
            var handler = QueueChange;
            if (handler != null) handler(newItem);
        }
        
        public void Enqueue(T item)
        {
            lock (_sync)
            {
                if (_items.Any(it => it.CompareTo(item) == 0))
                    return;

                _items.Enqueue(item);
                RaiseQueueChange(item);
            }
        }

        public T Dequeue()
        {
            lock (_sync)
            {
                return _items.Any() ? _items.Dequeue() : null;
            }
        }
    }
}
