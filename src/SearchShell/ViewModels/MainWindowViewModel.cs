﻿using System.Collections.ObjectModel;
using System.Linq;
using Catel.Data;
using Catel.MVVM.Services;
using Search.Core;

namespace SearchShell.ViewModels
{
    using Catel.MVVM;

    public class MainWindowViewModel : ViewModelBase
    {
        private readonly Search.Core.TextSearch _search;
        private readonly ISelectDirectoryService _directoryService;

        public MainWindowViewModel(ISelectDirectoryService directoryService)
        {
            _search = new TextSearch();
            _directoryService = directoryService;
            Folders = new ObservableCollection<string>();
        }

        public ObservableCollection<string> Folders
        {
            get { return GetValue<ObservableCollection<string>>(FoldersProperty); }
            set { SetValue(FoldersProperty, value); }
        }
        public static readonly PropertyData FoldersProperty = RegisterProperty("Folders", typeof(ObservableCollection<string>));

        
        public ObservableCollection<string> FoundItems
        {
            get { return GetValue<ObservableCollection<string>>(FoundedItemsProperty); }
            set { SetValue(FoundedItemsProperty, value); }
        }
        public static readonly PropertyData FoundedItemsProperty = RegisterProperty("FoundItems", typeof(ObservableCollection<string>));


        public int IndexCount
        {
            get { return GetValue<int>(IndexCountProperty); }
            set { SetValue(IndexCountProperty, value); }
        }
        public static readonly PropertyData IndexCountProperty = RegisterProperty("IndexCount", typeof(int));


        private Command _addFolderCommand;
        public Command AddFolderCommand
        {
            get 
            { 
                return _addFolderCommand ?? (_addFolderCommand = new Command(() =>
                {
                    if (!_directoryService.DetermineDirectory())
                        return;

                    var folderToIndex = _directoryService.DirectoryName;

                    _search.AddFolderOrFile(folderToIndex);
                    Folders.Add(folderToIndex);
                })); 
            }
        }
        
        private Command<string> _searchCommand;
        public Command<string> SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new Command<string>(query =>
                {
                    var foundItems = _search.Find(query);
                    FoundItems = foundItems != null ? new ObservableCollection<string>(foundItems) : null;
                    IndexCount = _search.IndexCount;
                },
                query => Folders.Any() ));
            }
        }

        protected override void OnClosing()
        {
            base.OnClosing();
            _search.Dispose();
        }
    }
}
