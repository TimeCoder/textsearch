﻿using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Search.Core.Test.FuncTests
{
    /// <summary>
    /// Функциональные тесты поиска
    /// </summary>
    [TestClass]
    public class SearchTest
    {
        /// <summary>
        /// Прогоняем базовый сценарий
        /// </summary>
        [TestMethod]
        public void TestBaseChain()
        {
            using (var search = new TextSearch())
            {
                // Папка для опытов
                var testFolder = Path.Combine(Path.GetTempPath(), "some_test_folder");
                Directory.CreateDirectory(testFolder);

                // Создаем файлы
                var documents = new[]
                {
                     new { Name = "file1.txt", Content = "мама мыла раму раму" },
                     new { Name = "file2.txt", Content = "мыло мыло раму" },
                     new { Name = "file3.txt", Content = "рама никого не мыла" }
                };
                
                foreach (var document in documents)
                {
                    var filename = Path.Combine(testFolder, document.Name);
                    Task.Run(() => File.WriteAllText(filename, document.Content));
                }

                // Запускаем индексацию
                search.AddFolderOrFile(testFolder);

                Thread.Sleep(2000); 
                
                // Поиск 1
                var result1 = search.Find("раму");

                Assert.IsNotNull(result1);
                Assert.AreEqual(2, result1.Count());
                Assert.IsTrue(result1.Count(doc => doc.Contains(documents[0].Name)) == 1);
                Assert.IsTrue(result1.Count(doc => doc.Contains(documents[1].Name)) == 1);

                // Поиск 2
                var result2 = search.Find("не");

                Assert.IsNotNull(result2);
                Assert.AreEqual(1, result2.Count());
                Assert.IsTrue(result2.Count(doc => doc.Contains(documents[2].Name)) == 1);

                // Изменяем файл
                File.WriteAllText(Path.Combine(testFolder, documents[0].Name), "корпускулярно-волновой дуализм");
                Thread.Sleep(2000);

                // Поиск 3
                var result3 = search.Find("корпускулярно-волновой");

                Assert.IsNotNull(result3);
                Assert.AreEqual(1, result3.Count());
                Assert.IsTrue(result3.Count(doc => doc.Contains(documents[0].Name)) == 1);

                Directory.Delete(testFolder, true);
            }
        }
    }
}
