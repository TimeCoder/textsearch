﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Search.Core.Index;
using Search.Core.Tokenizer;

namespace Search.Core.Test.UnitTests
{
    [TestClass]
    public class IndexerTest
    {
        [TestMethod]
        public void TestIndexDocument()
        {
            const string docName1 = "file.txt";
            const string docText1 = "мама мыла раму раму";

            const string docName2 = "file_2.txt";
            const string docText2 = "мыло мыло раму";

            const string docName3 = "файл_с_длинным_названием_на_русском_языке.txt";
            const string docText3 = "рама никого не мыла";
            
            var indexer = new Indexer();

            Task.WaitAll(
                Task.Run(() => indexer.IndexDocument(docName1, new StringReader(docText1), new WhitespaceTokenizer())),
                Task.Run(() => indexer.IndexDocument(docName2, new StringReader(docText2), new WhitespaceTokenizer())),
                Task.Run(() => indexer.IndexDocument(docName3, new StringReader(docText3), new WhitespaceTokenizer())));
            
            var result1 = indexer.Find("раму").ToList();

            Assert.AreEqual(2, result1.Count);
            Assert.IsTrue(result1.Contains(docName1));
            Assert.IsTrue(result1.Contains(docName2));


            var result2 = indexer.Find("не").ToList();

            Assert.AreEqual(1, result2.Count);
            Assert.IsTrue(result2.Contains(docName3));
        }
    }
}
