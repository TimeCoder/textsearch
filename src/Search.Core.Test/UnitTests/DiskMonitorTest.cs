﻿using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Search.Core.IO;

namespace Search.Core.Test.UnitTests
{
    [TestClass]
    public class DiskMonitorTest
    {
        [TestMethod]
        public void TestFileChange()
        {
            using (var monitor = new DiskMonitor())
            {
                var testFolder = Path.Combine(Path.GetTempPath(), "test_folder");
                Directory.CreateDirectory(testFolder);

                var wasEvent = false;

                monitor.OnFileNeedIndex += filename =>
                {
                    wasEvent = true;
                };

                monitor.AddFolderOrFile(testFolder, "*.*");

                var tmpFileName = Path.Combine(testFolder, "file1.txt");
                File.WriteAllText(tmpFileName, "содержимое файла");
                
                Thread.Sleep(1000);

                Assert.IsTrue(wasEvent, "Изменение файловое системы не было замечено.");

                Directory.Delete(testFolder, true);
            }
        }
    }
}
