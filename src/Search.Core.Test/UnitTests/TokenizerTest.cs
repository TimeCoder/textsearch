﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Search.Core.Tokenizer;

namespace Search.Core.Test.UnitTests
{
    [TestClass]
    public class TokenizerTest
    {
        [TestMethod]
        public void TestTokenize()
        {
            using (var stream = new StringReader("Один два три") )
            {
                var tokens = new WhitespaceTokenizer().Tokenize(stream).ToArray();

                Assert.IsNotNull(tokens);
                Assert.AreEqual(3, tokens.Length);

                Assert.AreEqual("Один", tokens[0]);
                Assert.AreEqual("два", tokens[1]);
                Assert.AreEqual("три", tokens[2]);
            }
        }
    }
}
